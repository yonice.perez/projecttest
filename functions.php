<?php
error_reporting(E_ALL);
ini_set('display_errors', 1 );

define('DB_NAME', 'usermanagment');
define('DB_USER', 'root');
define('DB_PASSWORD', 'Execute7784*');

$GLOBALS['DB'] = new PDO("mysql:host=localhost;dbname=".DB_NAME, DB_USER, DB_PASSWORD);

function authUser( $user, $password )
{
	$user = getUser($user);
	if(!$user)
	{
		return 0;
	}

		
	if( $user['password'] === $password)
	{
		
		//save session
		return 1;
	}
	else
	{
		
		return 2;
	}
}

function getUser( $user = '' ){
	if(!$user)return null;

	$stm   = $GLOBALS['DB']->query("SELECT * FROM user WHERE user='".$user."' ");
	return  $stm->fetch(PDO::FETCH_ASSOC);
}

function createUser( $user, $password, $permiso = 'alto' ){
	
	if(!$user || !$password) return false;

	$sql = "INSERT INTO user (user, password, permiso) VALUES (?,?,?)";
	$stmt= $GLOBALS['DB']->prepare($sql);
	$stmt->execute([$user, $password, $permiso]);

	return true;
}

function getUsers( $queryWhere = "" )
{
	$stm   = $GLOBALS['DB']->query("SELECT * FROM user ".$queryWhere);
	return  $stm->fetchAll();
	
}

function getPermisos(){

	return [
		'basico' => ['name' => 'Basico', 'roles' => ['acceso'] ],
		'medio' => ['name' => 'Medio' , 'roles' => ['acceso','consulta']],
		'medio_alto' => ['name' => 'Medio alto', 'roles' => ['acceso', 'agregar']],
		'alto_medio' => ['name' => 'Alto medio' , 'roles' => ['consulta', 'agregar']],//acceso?
		'alto' => ['name' => 'Alto' , 'roles' => ['acceso', 'consulta', 'agregar'] ],
	];
}

function getConsultaWhere(){
	$permisos = getPermisos();
	$user     = isset($_GET['user']) ? $_GET['user'] : false;
	$permiso  = isset($_GET['permiso']) ? $_GET['permiso'] : false;

	$queryWhere = "";

	if($user)
	{
		$queryWhere =" where user like '%".$user."%' ";
	}

	if(in_array($permiso, array_keys($permisos)))
	{
		$queryWhere = ($queryWhere) ?  
			$queryWhere." and permiso = '".$permiso."' " : 
			" where permiso = '".$permiso."' ";
	}

	return $queryWhere;
}