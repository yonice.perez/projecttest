function buscar(){
	
	let data = {
		user: $('#user').val(),
		permiso: $('#permiso').val()
	}
	
	window.location = 'consulta.php?'+$.param( data )
}

$(document).ready(function(){
	$("#permiso").on('change', function( e ){
		buscar();
	})
	$("#buscar").on('click', function(){
		buscar();
	} )
});
