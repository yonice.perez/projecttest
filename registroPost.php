<?php

require_once 'functions.php';

$user     = isset($_POST['user'])? $_POST['user'] : '';
$password = isset($_POST['password'])? $_POST['password'] : '';
$permiso = isset($_POST['permiso'])? $_POST['permiso'] : '';

if(getUser($user))
{
	//user exist
	header("Location: registro.php?error=1");

}
else{
	$new = createUser($user, $password, $permiso);
	//login?
	if($new)
	{
		header("Location: consulta.php");	
	}
	else{ //fails
		header("Location: registro.php?error=2");
	}
}
exit;