<?php
//im loggged
require_once 'functions.php';

$permisos = getPermisos();
$where    = getConsultaWhere();
$usuarios = getUsers($where);

$currentPermiso = isset($_GET['permiso']) ? $_GET['permiso'] : 'TODOS';
$currentUser    = isset($_GET['user']) ? $_GET['user'] : '';
?>

<!DOCTYPE html>
<html>
<head>
	<title>Project</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
	<div class="container mt-5">
		<div class="row bg-light ">
			<div class="col-md-6 pb-3">
				<div clas="form-group">
					<label>Usuario</label>
					<input id="user" type="text" class="form-control" name="user" value="<?= $currentUser?>" />
				</div>
				<div clas="form-group">
					<label>Permisos</label>
					<select id="permiso" required="true" class="form-control" name="permiso">
			        	<?php $selected = ( $currentPermiso ==='TODOS') ? "selected='selected'" : ''; ?>
			        		<option <?= $selected?> value="TODOS">Todos</option>
			        	
			        	<?php foreach($permisos as $key => $permiso) : 
			        		$selected = ( $currentPermiso ===$key) ? "selected='selected'" : '';
			        		?>
		        			<option <?= $selected?> value="<?= $key?>"><?= $permiso['name']?></option>
		        		<?php endforeach; ?>
		        	</select>
				</div>
			</div>
			<div class="col-md-6">
				<div clas="form-group">
					<button class="btn btn-info" id="buscar">BUSCAR</button>
				</div>
			</div>
		</div>
		<div class="row">
			<table class="table">
			  <thead>
			    <tr>
			    	 <th scope="col">Id</th>
			      <th scope="col">Usuario</th>
			      <th scope="col">Permiso</th>
			      <th scope="col">Roles</th>
			    </tr>
			  </thead>
			<tbody id="replaceContent">
				<?php foreach ($usuarios as $user)  : ?>
					<tr>
						<td><?= $user['id']?></td>
						<td><?= $user['user']?></td>
						<td><?= $permisos[$user['permiso']]['name']?></td>
						<td><?= implode(", ", $permisos[$user['permiso']]['roles'])?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
			</table>		
		</div>
	</div>
</body>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="consulta.js"></script>
</html>