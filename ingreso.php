<?php
require_once 'functions.php';
/**
 * 
 */
$usuario = isset($_POST['user']) ? $_POST['user'] : '';
$pass    = isset($_POST['password']) ? $_POST['password'] : '';


if(!$usuario || !$pass){
	header('Location: index.php');
	exit;
}

$auth = authUser($usuario, $pass);

if($auth == 1 ){
	header('Location: consulta.php');
}
else if($auth ==2 ){
	header('Location: index.php');
}else{
	header('Location: registro.php');
}
exit;