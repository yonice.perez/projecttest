<?php require_once 'functions.php' ?>
<!DOCTYPE html>
<html>
<head>
	<title>Project</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<?php 
	$usuario = isset($_GET['user']) ? $_GET['user'] : '';
	$error   = isset($_GET['error']) ? $_GET['error'] : '';
?>
<body>
	<div class="row">
		<div class="container">
			<pre>
				<?php if($error==1): ?>
				<h3>El usuario ingresado ya existe</h3>
			<?php endif; ?>
			<?php if($error==2): ?>
				<h3>Occurrio un error con el registro </h3>
			<?php endif; ?>
			</pre>
			<form autocomplete="off" class="form-signin mt-5 bg-light" action="registroPost.php" method="POST">
			     	<h3>Registrar usuario</h3>
			      <div class="form-label-group">
			      	 <label for="user">usuario</label>
			        <input name="user" value="<?= $usuario?>" type="text" id="user" class="form-control" required="true" autofocus="true">
			       
			      </div>

			      <div class="form-label-group">
			        <label for="inputPassword">Contrasena</label>
			        <input  name="password" type="text" id="inputPassword" class="form-control" placeholder=
			        "Password" required="true">
			        
			      </div>

			       <div class="form-label-group mb-2">
			        	<label for="inputPassword">Permiso</label>
			        	<select required="true" class="form-control" name="permiso">
			        		<?php foreach(getPermisos() as $key => $permiso) : ?>
			        			<option value="<?= $key?>"><?= $permiso['name']?></option>
			        		<?php endforeach; ?>
			        	</select>
			      </div>

			      <button class="btn btn-lg btn-primary btn-block" type="submit">Alta</button>
			    </form>
		</div>
	</div>
</body>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</html>